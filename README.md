# free-learn-mobile-applications
## Name
Free-learn

## Description
Free-learn is a Java mobile application that allows you to search for thousands of books. The application consists on several interfaces/screens which will allow you to log in, search for books, filter your results and set alarms for you to be able to set times to read.

## Installation
To try the application, just import the project in Android Studio and run it as an application. Emulator used: pixel_3a_API_30_x86 Android 11 API 30

## Usage
First, the main page will be loaded, in wich a set of books about learning will be loaded. In the upper-right corner, you will see the options button (three white points vertically displayed). Among this options you will see reminders, alarms, settings or filters.
The reminders interface will allow you to set a reminder at a given date and time so that you can program your study.
The alarms interface makes possible to set a timer (hours and minutes).
Finally, the filters interface allows to filter your search through the selection of some filters, such as title, order by, author or category.

## Contributing
Anyone interested in developing the application in more detail is invited to do so. Please, contact me through e-mail (See Contact).

##Links
https://youtu.be/hYK9-XuBf4Y

## Authors and acknowledgment
By Adrián F. Brenes Martínez & Daniel Paniagua Urquijo.

##Contact
E-mail: adrianbrenes2000@protonmail.com

## Project status
Unfinished. This was supposed to be a group project between three people, but a week before the completion date one of the group members quited the subject, so his part could not be used at all, that is why the project is incomplete. Nevertheless, I invite you take an insight into the project and check how the info from the Google books API is requested and retrieved. 

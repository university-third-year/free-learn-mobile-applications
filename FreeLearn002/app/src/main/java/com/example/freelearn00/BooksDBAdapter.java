package com.example.freelearn00;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BooksDBAdapter {
    private static final String TAG ="APPMOV: BookBD";

    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "books";
    private static final int DATABASE_VERSION = 2;

    public static final String KEY_TITLE = "title";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_DESC = "descr";
    public static final String KEY_PRICE = "price";
    public static final String KEY_ID = "_id";

    private static final String DATABASE_CREATE = "create table " + DATABASE_TABLE + " (" +
            KEY_ID +" integer primary key autoincrement, "+
            KEY_TITLE +" text not null, "+
            KEY_AUTHOR +" text not null,"+
            KEY_PRICE +" text not null,"+
            KEY_DESC +" text not null);";

    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }

    public BooksDBAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public BooksDBAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public long createBook(String title, String author, String description, String price){
        ContentValues initialValues=new ContentValues();
        initialValues.put(KEY_TITLE,title);
        initialValues.put(KEY_AUTHOR,author);
        initialValues.put(KEY_DESC,description);
        initialValues.put(KEY_PRICE,price);

        return mDb.insert(DATABASE_TABLE,null,initialValues);
    }

    public boolean deleteBook(long rowId){
        return mDb.delete(DATABASE_TABLE,KEY_ID+"="+rowId,null)>0;
    }

    public Cursor fetchAllBooks(){
        return mDb.query(DATABASE_TABLE, new String[]{KEY_ID,KEY_TITLE,KEY_AUTHOR,KEY_DESC,KEY_PRICE},
                null,null,null,null,null);
    }

    public Cursor fetchNote(long rowId) throws SQLException{
        Cursor mCursor=
                mDb.query(true,DATABASE_TABLE,new String[]{ KEY_ID,KEY_TITLE,KEY_AUTHOR,KEY_DESC,KEY_PRICE},
                        KEY_ID+"="+rowId,null,null,null,null,null);
        if(mCursor!=null){
            mCursor.moveToFirst();
        }
        return mCursor;
    }
}

package com.example.freelearn00;

import android.content.Intent;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
public class UsuarioActivity extends AppCompatActivity{
    ImageButton buttStar, buttHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);


        buttStar = (ImageButton)findViewById(R.id.buttStar);
        buttHome = (ImageButton)findViewById(R.id.buttHome);

        buttStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UsuarioActivity.this, FavActivity.class);
                startActivity(intent);
            }
        });

        buttHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UsuarioActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}

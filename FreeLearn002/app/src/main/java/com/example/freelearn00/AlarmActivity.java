package com.example.freelearn00;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.Calendar;

public class AlarmActivity extends AppCompatActivity {
    Button selHour_al, confir;
    TextView yHour, yMinute;
    private int currentHour, currentMinutes;

    Intent intent = null;

    Calendar calendar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_activity);

        yHour = findViewById(R.id.hour_alarm);
        yMinute = findViewById(R.id.minutes_alarm);
        selHour_al = findViewById(R.id.sel_hour_al);
        confir = findViewById(R.id.set_alarm);

        selHour_al.setOnClickListener(v ->  {
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            currentMinutes = calendar.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(AlarmActivity.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
                    yHour.setText(String.format("%02d", hourOfDay));
                    yMinute.setText(String.format("%02d", minutes));
                }
            }, currentHour, currentMinutes, true);
            timePickerDialog.show();
        });

        confir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!yHour.getText().toString().isEmpty() && !yMinute.getText().toString().isEmpty()){
                    Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);

                    intent.putExtra(AlarmClock.EXTRA_HOUR, Integer.parseInt(yHour.getText().toString()));
                    intent.putExtra(AlarmClock.EXTRA_MINUTES, Integer.parseInt(yMinute.getText().toString()));

                    intent.putExtra(AlarmClock.EXTRA_MESSAGE, "FreeLearn");


                    if(intent.resolveActivity(getPackageManager())!=null){
                        startActivity(intent);
                    }else{
                        Toast.makeText(AlarmActivity.this, "No hay ninguna aplicación que soporte esta acción", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(AlarmActivity.this, "Introduzca una hora", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
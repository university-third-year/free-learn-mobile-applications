package com.example.freelearn00;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.AsyncTask;
import android.util.JsonReader;
import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import android.widget.ArrayAdapter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

class Volume{
    private String title;
    private ArrayList<String> authors;
    private String publisher;
    private String publishedDate;
    private String description;
    private int pageCount;
    private String height;
    private String width;
    private String thickness;
    private String printType;
    private String mainCategory;
    private ArrayList<String> categories;
    private double averageRating;
    private int ratingsCount;
    private String volumeId;
    private String imageLink;
    private String language;
    private String infoLink;
    private String eTag;
    private String selfLink;
    private String saleInfoCountry;
    private String saleInfoSaleability;
    private boolean saleInfoIsEbook;
    private String saleInfoPrice;
    private String saleInfoCurrency;
    private double retailPrice;
    private String retailCurrency;
    private String buyLink;
    private String accessInfoCountry;
    private String accessInfoViewability;
    private boolean accessInfoEmbeddable;
    private String accessInfoTextToSpeechPermission;
    private boolean accessInfoEpubIsAvailable;
    private String accessInfoEpubAcsTokenLink;
    private boolean accessInfoPdfIsAvailable;
    private String accessViewStatus;

    public Volume(){
        this.title="";
        this.authors=new ArrayList<String>();
        this.publisher="";
        this.publishedDate="";
        this.description="";
        this.volumeId="";
        this.pageCount=0;
        this.height="";
        this.width="";
        this.thickness="";
        this.printType="";
        this.mainCategory="";
        this.categories=new ArrayList<String>();
        this.averageRating=0;
        this.ratingsCount=0;
        this.eTag="";
        this.imageLink="";
        this.language="";
        this.infoLink="";
        this.selfLink="";
        this.saleInfoCountry="";
        this.saleInfoSaleability="";
        this.saleInfoIsEbook=false;
        this.saleInfoPrice="";
        this.saleInfoCurrency="";
        this.retailPrice=0;
        this.retailCurrency="";
        this.buyLink="";
        this.accessInfoCountry="";
        this.accessInfoViewability="";
        this.accessInfoEmbeddable=false;
        this.accessInfoTextToSpeechPermission="";
        this.accessInfoEpubIsAvailable=false;
        this.accessInfoEpubAcsTokenLink="";
        this.accessInfoPdfIsAvailable=false;
        this.accessViewStatus="";
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public void setVolumeId(String volumeId) {
        this.volumeId = volumeId;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public void setRatingsCount(int ratingsCount) {
        this.ratingsCount = ratingsCount;
    }

    public String geteTag() {
        return eTag;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public void setLanguage(String language) {
        this.language = language;
    }


    public void setSaleInfoCountry(String saleInfoCountry) {
        this.saleInfoCountry = saleInfoCountry;
    }

    public void setSaleInfoSaleability(String saleInfoSaleability) {
        this.saleInfoSaleability = saleInfoSaleability;
    }

    public void setSaleInfoIsEbook(boolean saleInfoIsEbook) {
        this.saleInfoIsEbook=saleInfoIsEbook;

    }

    public void setSaleInfoPrice(String saleInfoPrice) {
        this.saleInfoPrice = saleInfoPrice;
    }

    public void setSaleInfoCurrency(String saleInfoCurrency) {
        this.saleInfoCurrency = saleInfoCurrency;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public void setRetailCurrency(String retailCurrency) {
        this.retailCurrency = retailCurrency;
    }

    public void setBuyLink(String buyLink) {
        this.buyLink = buyLink;
    }

    public void setAccessInfoCountry(String accessInfoCountry) {
        this.accessInfoCountry = accessInfoCountry;
    }

    public void setAccessInfoViewability(String accessInfoViewability) {
        this.accessInfoViewability = accessInfoViewability;
    }

    public void setAccessInfoEmbeddable(boolean accessInfoEmbeddable) {
        this.accessInfoEmbeddable=accessInfoEmbeddable;
    }

    public void setAccessInfoTextToSpeechPermission(String accessInfoTextToSpeechPermission) {
        this.accessInfoTextToSpeechPermission = accessInfoTextToSpeechPermission;
    }

    public void setAccessInfoEpubIsAvailable(boolean accessInfoEpubIsAvailable) {
        this.accessInfoEpubIsAvailable=accessInfoEpubIsAvailable;
    }

    public void setAccessInfoEpubAcsTokenLink(String accessInfoEpubAcsTokenLink) {
        this.accessInfoEpubAcsTokenLink = accessInfoEpubAcsTokenLink;
    }

    public void setAccessInfoPdfIsAvailable(boolean accessInfoPdfIsAvailable) {
        this.accessInfoPdfIsAvailable=accessInfoPdfIsAvailable;
    }

    public void setAccessViewStatus(String accessViewStatus) {
        this.accessViewStatus = accessViewStatus;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public void setThickness(String thickness) {
        this.thickness = thickness;
    }

    public void setPrintType(String printType) {
        this.printType = printType;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }



    public void setImageLinks(String imageLink) {
        this.imageLink = imageLink;
    }

    public void setInfoLink(String infoLink) {
        this.infoLink = infoLink;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public String getDescription() {
        return description;
    }

    public String getVolumeId() {
        return volumeId;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getThickness() {
        return thickness;
    }

    public String getPrintType() {
        return printType;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public int getRatingsCount() {
        return ratingsCount;
    }



    public String getImageLink() { return imageLink; }

    public String getLanguage() {
        return language;
    }

    public String getInfoLink() {
        return infoLink;
    }



    public String getSaleInfoCountry() {
        return saleInfoCountry;
    }

    public String getSaleInfoSaleability() {
        return saleInfoSaleability;
    }

    public boolean getSaleInfoIsEbook() {
        return saleInfoIsEbook;
    }

    public String getSaleInfoPrice() {
        return saleInfoPrice;
    }

    public String getSaleInfoCurrency() {
        return saleInfoCurrency;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public String getRetailCurrency() {
        return retailCurrency;
    }

    public String getBuyLink() {
        return buyLink;
    }

    public String getAccessInfoCountry() {
        return accessInfoCountry;
    }

    public String getAccessInfoViewability() {
        return accessInfoViewability;
    }

    public boolean getAccessInfoEmbeddable() {
        return accessInfoEmbeddable;
    }

    public String getAccessInfoTextToSpeechPermission() {
        return accessInfoTextToSpeechPermission;
    }

    public boolean getAccessInfoEpubIsAvailable() {
        return accessInfoEpubIsAvailable;
    }

    public String getAccessInfoEpubAcsTokenLink() {
        return accessInfoEpubAcsTokenLink;
    }

    public boolean getAccessInfoPdfIsAvailable() {
        return accessInfoPdfIsAvailable;
    }

    public String getAccessViewStatus() {
        return accessViewStatus;
    }
    public void addAuthor(String author){
        authors.add(author);
    }
    public void addCategory(String cat){
        categories.add(cat);
    }
}
class Search{
    private String q;
    private String intitle;
    private String inauthor;
    private String inpublisher;
    private String subject;
    private String filter;
    private String orderby;
    private String volumeID;
    private String lang;

    public Search (){
        this.q="";
        this.intitle="";
        this.inauthor="";
        this.inpublisher="";
        this.subject="";
        this.filter="";
        this.orderby="";
        this.volumeID="";
        this.lang="";
    }
    public String getLang(){return lang;}
    public String getQ() {
        return q;
    }

    public String getIntitle() {
        return intitle;
    }

    public String getInauthor() {
        return inauthor;
    }

    public String getInpublisher() {
        return inpublisher;
    }

    public String getSubject() {
        return subject;
    }

    public String getFilter() {
        return filter;
    }

    public String getOrderby() {
        return orderby;
    }

    public String getVolumeID() {
        return volumeID;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public void setIntitle(String intitle) {
        this.intitle = intitle;
    }

    public void setInauthor(String inauthor) {
        this.inauthor = inauthor;
    }

    public void setInpublisher(String inpublisher) {
        this.inpublisher = inpublisher;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    public void setVolumeID(String volumeID) {
        this.volumeID = volumeID;
    }
    public void setLang(String lang){this.lang=lang;}
}

public class MainActivity extends AppCompatActivity {

    final String GOOGLE_KEY = "AIzaSyAfrG3PrvquTbtsUH-ckpWRPpNYrscUOMk";
    ImageButton buttStar, buttUsuario;
    Button buttIrALibro;
    Search search = new Search();
    TextView tv;
    private ListView m_listview, m_listview_2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //search = "https://www.googleapis.com/books/v1/volumes?q=learn&maxResults=6&key="+GOOGLE_KEY;
        m_listview=(ListView) findViewById(R.id.id_list_view);
        m_listview_2=(ListView) findViewById(R.id.id_list_view_2);

        buttStar = (ImageButton)findViewById(R.id.buttStar);
        buttUsuario = (ImageButton)findViewById(R.id.buttUsuario);
        //buttIrALibro=(Button)findViewById(R.id.buttonGoToBook);

        Context context = getApplicationContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        setFilters(prefs, search);


        buttStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FavActivity.class);
                startActivity(intent);
            }
        });

        buttUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UsuarioActivity.class);
                startActivity(intent);
            }
        });
        /*buttIrALibro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, VolumeActivity.class);
                startActivity(intent);
            }
        });*/
        new GoogleBooks().execute();
    }
    private class GoogleBooks extends AsyncTask<View,Void,ArrayList<Volume>>{
        @Override
        protected ArrayList<Volume> doInBackground(View... urls){
            ArrayList<Volume> temp;
            temp=makeCall(buildSearch(search));
            return temp;
        }

        @Override
        protected void onPostExecute(ArrayList<Volume> result){
            List<String> listTitle=new ArrayList<String>();
            List<String> listId=new ArrayList<String>();

            for (int i=0; i<result.size();i++){
                /*if(!result.get(i).getImageLink().equals("")){
                    listTitle.add(i,result.get(i).getImageLink());
                }else{
                    listTitle.add(i,"undefined");
                }*/
                listTitle.add(i,result.get(i).getTitle());
                if(!result.get(i).getTitle().equals("")){
                    listTitle.add(i,result.get(i).getTitle());
                }else{
                    listTitle.add(i,"undefined");
                }
                if(!result.get(i).getVolumeId().equals("")){
                    listId.add(i,result.get(i).getVolumeId());
                }else{
                    listId.add(i,"undefined");
                }
            }

            ArrayAdapter<String> titleAdapter;
            ArrayAdapter<String> authAdapter;

            titleAdapter=new ArrayAdapter<String>(MainActivity.this,R.layout.row_layout,R.id.title,listTitle);
            //idAdapter=new ArrayAdapter<String>(MainActivity.this,R.layout.row_layout,R.id.author,listId);

            m_listview.setAdapter(titleAdapter);
            //m_listview_2.setAdapter(idAdapter);

        }

    }
    public static ArrayList<Volume> makeCall(String stringURL){
        URL url=null;
        System.out.println(stringURL);
        BufferedInputStream is =null;
        JsonReader jsonreader;
        ArrayList<Volume> temp=new ArrayList<Volume>();

        try {
            url = new URL(stringURL);
        } catch (Exception ex) {
            System.out.println("Malformed URL");
        }

        try {
            if (url != null) {
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                is = new BufferedInputStream(urlConnection.getInputStream());
            }
        } catch (IOException ioe) {
            System.out.println("IOException");
        }

        if(is!=null){
            try{
                jsonreader=new JsonReader(new InputStreamReader(is,"UTF-8"));
                jsonreader.beginObject();
                while (jsonreader.hasNext()){
                    String name =jsonreader.nextName();
                    //buscamos "items
                    if(name.equals("items")){
                        jsonreader.beginArray();
                        while(jsonreader.hasNext()){
                            Volume vol=new Volume();
                            jsonreader.beginObject();
                            while (jsonreader.hasNext()){
                                name=jsonreader.nextName();
                                if(name.equals("id")){
                                    vol.setVolumeId(jsonreader.nextString());
                                }else if(name.equals("etag")){
                                    vol.seteTag(jsonreader.nextString());
                                }else if(name.equals("selfLink")){
                                    vol.setSelfLink(jsonreader.nextString());
                                }else if(name.equals("volumeInfo")){
                                    jsonreader.beginObject();
                                    while (jsonreader.hasNext()){
                                        name=jsonreader.nextName();
                                        if(name.equals("title")){
                                            vol.setTitle(jsonreader.nextString());
                                        }else if(name.equals("authors")){
                                            jsonreader.beginArray();
                                            while(jsonreader.hasNext()){
                                                name=jsonreader.nextString();
                                                vol.addAuthor(name);
                                            }
                                            jsonreader.endArray();
                                        }else if(name.equals("publisher")){
                                            vol.setPublisher(jsonreader.nextString());
                                        }else if(name.equals("publishedDate")){
                                            vol.setPublishedDate(jsonreader.nextString());
                                        }else if(name.equals("description")){
                                            vol.setDescription(jsonreader.nextString());
                                        }else if(name.equals("pageCount")){
                                            vol.setPageCount(Integer.parseInt(jsonreader.nextString()));
                                        }else if(name.equals("dimensions")){
                                            jsonreader.beginObject();
                                            while (jsonreader.hasNext()){
                                                name=jsonreader.nextName();
                                                if(name.equals("height")){
                                                    vol.setHeight(jsonreader.nextString());
                                                }else if(name.equals("width")){
                                                    vol.setWidth(jsonreader.nextString());
                                                }else if(name.equals("thickness")){
                                                    vol.setThickness(jsonreader.nextString());
                                                }else{
                                                    jsonreader.skipValue();
                                                }
                                            }
                                            jsonreader.endObject();
                                        }else if(name.equals("printType")){
                                            vol.setPrintType(jsonreader.nextString());
                                        }else if(name.equals("mainCategory")){
                                            vol.setMainCategory(jsonreader.nextString());
                                        }else if(name.equals("categories")){
                                            jsonreader.beginArray();
                                            while (jsonreader.hasNext()){
                                                name=jsonreader.nextString();
                                                vol.addCategory(name);
                                            }
                                            jsonreader.endArray();
                                        }else if(name.equals("averageRating")){
                                            vol.setAverageRating(Double.parseDouble(jsonreader.nextString()));
                                        }else if(name.equals("ratingsCount")){
                                            vol.setRatingsCount(Integer.parseInt(jsonreader.nextString()));
                                        }else if(name.equals("imageLinks")){
                                            jsonreader.beginObject();
                                            while (jsonreader.hasNext()){
                                                name=jsonreader.nextName();
                                                if(name.equals("smallThumbnail")){
                                                    vol.setImageLinks(jsonreader.nextString());
                                                }else {
                                                    jsonreader.skipValue();
                                                }
                                            }
                                            jsonreader.endObject();
                                        }else if(name.equals("language")){
                                            vol.setLanguage(jsonreader.nextString());
                                        }else if(name.equals("infoLink")){
                                            vol.setInfoLink(jsonreader.nextString());
                                        }else {
                                            jsonreader.skipValue();
                                        }
                                    }
                                    jsonreader.endObject();
                                }else if(name.equals("saleInfo")){
                                    jsonreader.beginObject();
                                    while(jsonreader.hasNext()){
                                        name=jsonreader.nextName();
                                        if(name.equals("country")){
                                            vol.setSaleInfoCountry(jsonreader.nextString());
                                        }else if(name.equals("saleability")){
                                            vol.setSaleInfoSaleability(jsonreader.nextString());
                                        }else if(name.equals("isEbook")){
                                            vol.setSaleInfoIsEbook(jsonreader.nextBoolean());
                                        }else if(name.equals("listPrice")){
                                            jsonreader.beginObject();
                                            while(jsonreader.hasNext()){
                                                name=jsonreader.nextName();
                                                if(name.equals("amount")){
                                                    vol.setSaleInfoPrice(jsonreader.nextString());
                                                }else if(name.equals("currencyCode")){
                                                    vol.setSaleInfoCurrency(jsonreader.nextString());
                                                }else{
                                                    jsonreader.skipValue();
                                                }
                                            }
                                            jsonreader.endObject();
                                        }else if(name.equals("retailPrice")){
                                            jsonreader.beginObject();
                                            while ((jsonreader.hasNext())){
                                                name=jsonreader.nextName();
                                                if(name.equals("amount")){
                                                    vol.setRetailPrice(Double.parseDouble(jsonreader.nextString()));
                                                }else if(name.equals("currencyCode")){
                                                    vol.setRetailCurrency(jsonreader.nextString());
                                                }else{
                                                    jsonreader.skipValue();
                                                }
                                            }
                                            jsonreader.endObject();
                                        }else if(name.equals("buyLink")){
                                            vol.setBuyLink(jsonreader.nextString());
                                        }else{
                                            jsonreader.skipValue();
                                        }
                                    }
                                    jsonreader.endObject();
                                }else if(name.equals("accessInfo")){
                                    jsonreader.beginObject();
                                    while (jsonreader.hasNext()){
                                        name=jsonreader.nextName();
                                        if(name.equals("country")){
                                            vol.setAccessInfoCountry(jsonreader.nextString());
                                        }else if(name.equals("viewability")){
                                            vol.setAccessInfoViewability(jsonreader.nextString());
                                        }else if(name.equals("embeddable")){
                                            vol.setAccessInfoEmbeddable(jsonreader.nextBoolean());
                                        }else if(name.equals("textToSpeechPermission")){
                                            vol.setAccessInfoTextToSpeechPermission(jsonreader.nextString());
                                        }else if(name.equals("epub")){
                                            jsonreader.beginObject();
                                            while(jsonreader.hasNext()){
                                                name=jsonreader.nextName();
                                                if(name.equals("isAvailable")){
                                                    vol.setAccessInfoEpubIsAvailable(jsonreader.nextBoolean());
                                                }else if(name.equals("acsTokenLink")){
                                                    vol.setAccessInfoEpubAcsTokenLink(jsonreader.nextString());
                                                }else{
                                                    jsonreader.skipValue();
                                                }
                                            }
                                            jsonreader.endObject();
                                        }else if(name.equals("pdf")){
                                            jsonreader.beginObject();
                                            while (jsonreader.hasNext()){
                                                name=jsonreader.nextName();
                                                if(name.equals("isAvailable")){
                                                    vol.setAccessInfoPdfIsAvailable(jsonreader.nextBoolean());
                                                }else{
                                                    jsonreader.skipValue();
                                                }
                                            }
                                            jsonreader.endObject();
                                        }else if(name.equals("accessViewStatus")){
                                            vol.setAccessViewStatus(jsonreader.nextString());
                                        }else{
                                            jsonreader.skipValue();
                                        }
                                    }
                                    jsonreader.endObject();
                                }else{
                                    jsonreader.skipValue();
                                }
                            }
                            jsonreader.endObject();
                            temp.add(vol);
                        }
                        jsonreader.endArray();
                    }else {
                        jsonreader.skipValue();
                    }
                }
                jsonreader.endObject();
            }catch (Exception e) {
                System.out.println("Exception");
                return new ArrayList<Volume>();
            }
        }

        return temp;
    }
    public String buildSearch(Search s){
        char comillas='"';
        String tmp;
        if(!s.getVolumeID().equals("")){
            tmp="https://www.googleapis.com/books/v1/volumes/"+s.getVolumeID()+"?key="+GOOGLE_KEY;
            return tmp;
        }
        tmp="https://www.googleapis.com/books/v1/volumes?q=";
        if(!s.getQ().equals("")){
            tmp=tmp+comillas+s.getQ()+comillas;
        }else{
            tmp=tmp+"learn";
        }if(!s.getIntitle().equals("")){
            tmp=tmp+"+intitle:"+comillas+s.getIntitle()+comillas;
        }
        if(!s.getInauthor().equals("")){
            tmp=tmp+"+inauthor:"+comillas+s.getInauthor()+comillas;
        }
        if(!s.getInpublisher().equals("")){
            tmp=tmp+"+inpublisher:"+comillas+s.getInpublisher()+comillas;
        }
        if(!s.getSubject().equals("")){
            tmp=tmp+"+subject:"+s.getSubject();
        }
        if(!s.getFilter().equals("")){
            if(s.getFilter().equals("Gratuito")){
                tmp=tmp+"&filter=free-ebooks";
            }else if(s.getFilter().equals("De pago")){
                tmp=tmp+"&filter=paid-ebooks";
            }else if(s.getFilter().equals("Ebook")){
                tmp=tmp+"&filter=ebooks";
            }
        }
        if(!s.getLang().equals("")){
            if(s.getLang().equals("Español")){
                tmp=tmp+"&langRestrict=es";
            }else if(s.getLang().equals("Inglés")){
                tmp=tmp+"&langRestrict=en";
            }else if(s.getLang().equals("Alemán")){
                tmp=tmp+"&langRestrict=de";
            }else if(s.getLang().equals("Francés")){
                tmp=tmp+"&langRestrict=fr";
            }
            else if(s.getLang().equals("Chino")){
                tmp=tmp+"&langRestrict=zh";

            }
        }
        if(!s.getOrderby().equals("")) {
            if (s.getOrderby().equals("Novedades")) {
                tmp = tmp + "&orderBy=newest";
            } else if (s.getOrderby().equals("Relevancia")) {
                tmp = tmp + "&orderBy=relevance";
            }
        }
        tmp=tmp+"&maxResults=40&key="+GOOGLE_KEY;
        System.out.println(tmp);
        return tmp;
    }
    public void setFilters(SharedPreferences fPreferences, Search s){
        if(fPreferences.contains("SEARCH")){
            s.setQ(fPreferences.getString("SEARCH",""));
        }
        if(fPreferences.contains("ORDER")){
            s.setOrderby(fPreferences.getString("ORDER",""));
        }
        if(fPreferences.contains("AUTHOR")){
            s.setInauthor(fPreferences.getString("AUTHOR",""));
        }
        if(fPreferences.contains("PUBLISHER")){
            s.setInpublisher(fPreferences.getString("PUBLISHER",""));
        }
        if(fPreferences.contains("CATEGORY")){
            s.setSubject(fPreferences.getString("CATEGORY",""));
        }
        if(fPreferences.contains("PRICE")){
            s.setFilter(fPreferences.getString("PRICE",""));
        }
        if(fPreferences.contains("FORMAT")){
            s.setFilter(fPreferences.getString("FORMAT",""));
        }
        if(fPreferences.contains("LANG")){
            s.setLang(fPreferences.getString("LANG",""));
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_calendar){
            Intent intent = new Intent(MainActivity.this, CalendarActivity.class);
            startActivity(intent);
        }

        if(id == R.id.action_alarm){
            Intent intent = new Intent(MainActivity.this, AlarmActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_settings) {
            System.out.println("APPMOV: Settings action en actividad MainActivity...");
            return true;
        }

        if (id == R.id.action_about) {
            System.out.println("APPMOV: About action en actividad MainActivity...");
            return true;
        }
        if(id==R.id.action_filter){
            System.out.println("APPMOV: Filter action en actividad MainActivity...");
            Intent intent=new Intent(MainActivity.this,FiltersActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void onResume(){
        super.onResume();

        Context context = getApplicationContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String title = prefs.getString("SEARCH", "");
        String order = prefs.getString("ORDER","");
        String author = prefs.getString("AUTHOR","");
        String publisher = prefs.getString("PUBLISHER","");
        String category = prefs.getString("CATEGORY","");
        String price = prefs.getString("PRICE","");
        String format = prefs.getString("FORMAT","");
        String languaje = prefs.getString("LANG","");

        setFilters(prefs, search);
    }
}
// La parte de bordershadow está cogida de http://www.worldbestlearningcenter.com/tips/Android-linearlayout-border-shadow.htm
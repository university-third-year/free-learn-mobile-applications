package com.example.freelearn00;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarActivity extends AppCompatActivity{
    Button seleDate, seleHour;
    TextView yDate, yHour;
    Button save;

    Calendar actual = Calendar.getInstance();
    Calendar calendar = Calendar.getInstance();

    Intent intent = null;

    private int hour, minutes, day, month, year;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity);

        seleDate = findViewById(R.id.sele_date);
        seleHour = findViewById(R.id.sele_hour);
        yDate = findViewById(R.id.y_date);
        yHour = findViewById(R.id.y_hour);
        save = findViewById(R.id.btn_save);

        seleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                year = actual.get(Calendar.YEAR);
                month = actual.get(Calendar.MONTH);
                day = actual.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        calendar.set(Calendar.DAY_OF_MONTH, day);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.YEAR, year);

                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        String stringDate = format.format(calendar.getTime());
                        yDate.setText(stringDate);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        seleHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hour = actual.get(Calendar.HOUR_OF_DAY);
                minutes = actual.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(v.getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);

                        yHour.setText(String.format("%02d:%02d", hourOfDay, minute));
                    }
                }, hour, minutes, true);
                timePickerDialog.show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CalendarActivity.this,"Recordatorio guardado", Toast.LENGTH_SHORT).show();
                intent = new Intent(intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");

                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calendar.getTimeInMillis());
                intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calendar.getTimeInMillis() + 60 * 60 * 1000);

                intent.putExtra(CalendarContract.Events.TITLE,"FreeLearn");

                startActivity(intent);
            }
        });
    }

}

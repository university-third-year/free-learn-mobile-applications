package com.example.freelearn00;

import android.content.Intent;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class FavActivity extends AppCompatActivity {
    ImageButton buttUsuario, buttHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fav_activity);

        buttUsuario = (ImageButton)findViewById(R.id.buttUsuario);
        buttHome = (ImageButton)findViewById(R.id.buttHome);

        buttUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FavActivity.this, UsuarioActivity.class);
                startActivity(intent);
            }
        });

        buttHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FavActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

}
